package com.iteso.test;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.iteso.test.beans.ItemProduct;

public class ActivityDetail extends AppCompatActivity {

    EditText title;
    EditText store;
    EditText location;
    ImageView image;
    ItemProduct itemProduct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        itemProduct = getIntent().getParcelableExtra("ITEM");
        title = (EditText) findViewById(R.id.activity_detail_title);
        store = (EditText) findViewById(R.id.activity_detail_store);
        location = (EditText) findViewById(R.id.activity_detail_location);
        image = (ImageView) findViewById(R.id.activity_detail_image);
        title.setText(itemProduct.getTitle());
        store.setText(itemProduct.getStore().getName());
        location.setText(itemProduct.getStore().getCity().getName());
        switch (itemProduct.getImage()) {
            case 0:
                image.setImageResource(R.drawable.mac);
                break;
            case 1:
                image.setImageResource(R.drawable.alienware);
                break;
            case 2:
                image.setImageResource(R.drawable.mac);
                break;
        }
    }
    public void onClick(View v) {
        if (v.getId() == R.id.activity_detail_save) {
            itemProduct.setTitle(title.getText().toString());
            itemProduct.getStore().setName(store.getText().toString());
            itemProduct.getStore().getCity().setName(location.getText().toString());
            Intent intent = new Intent();
            intent.putExtra("ITEM", itemProduct);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }
}
