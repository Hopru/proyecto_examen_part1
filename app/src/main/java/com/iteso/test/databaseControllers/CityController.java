package com.iteso.test.databaseControllers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iteso.test.DataBaseHandler;
import com.iteso.test.beans.Category;
import com.iteso.test.beans.City;

public class CityController {
    public long addCity(City city, DataBaseHandler dh) {
        long inserted = 0;
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_CITY_NAME, city.getName());
        // Inserting Row
        inserted = db.insert(DataBaseHandler.TABLE_CITY, null, values);
        // Closing database connection
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        values = null;
        return inserted;
    }

    public void deleteCity(int idCity, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        db.delete(DataBaseHandler.TABLE_CITY, DataBaseHandler.KEY_CITY_ID + " = ?", new String[]{String.valueOf(idCity)});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
    }

    public int updateCity(City city, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_CITY_NAME, city.getName());
        // Updating row
        int count = db.update(DataBaseHandler.TABLE_CITY, values, DataBaseHandler.KEY_CITY_ID + " = ?", new String[]{String.valueOf(city.getIdCity())});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        return count;
    }

    public City getCityById(int idCity, DataBaseHandler dh) {
        City city = new City();
        String selectQuery = "SELECT  C." + DataBaseHandler.KEY_CITY_ID + ", C." + DataBaseHandler.KEY_CITY_NAME + " FROM " + DataBaseHandler.TABLE_CITY + " C WHERE C." + DataBaseHandler.KEY_CATEGORY_ID + "= " + idCity;
        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            city.setIdCity(cursor.getInt(0));
            city.setName(cursor.getString(1));
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }
        db = null;
        cursor = null;
        // return store
        return city;
    }
}
