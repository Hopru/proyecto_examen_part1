package com.iteso.test.databaseControllers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.iteso.test.DataBaseHandler;

public class MyContentProvider extends ContentProvider {

    private static final String PROVIDER_NAME = "com.iteso.test.myitesoitems";
    private static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/products");
    private static final int PRODUCTS = 1;
    private static final int PRODUCTS_ID = 2;
    private static final UriMatcher uriMatcher = getUriMatcher();

    private static UriMatcher getUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "products", PRODUCTS);
        uriMatcher.addURI(PROVIDER_NAME, "products/*", PRODUCTS_ID);
        return uriMatcher;
    }

    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case PRODUCTS:
                return "vnd.android.cursor.dir/vnd.com.iteso.test.myitesoitems.provider.products";
            case PRODUCTS_ID:
                return "vnd.android.cursor.item/vnd.com.iteso.test.myitesoitems.provider.products";
        }
        return "";
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        DataBaseHandler dataBaseHandler = DataBaseHandler.getInstance(context);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        String category = null;
        Context context = getContext();
        DataBaseHandler dataBaseHandler = DataBaseHandler.getInstance(context);

        if (uriMatcher.match(uri) == PRODUCTS_ID) {
            //Query is for one single image. Get the ID from the URI.
            category = "'" + uri.getPathSegments().get(1) + "'";
        }
        return ItemProductControl.getProductsByCategoryCursor(category, dataBaseHandler);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
