package com.iteso.test.databaseControllers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iteso.test.DataBaseHandler;
import com.iteso.test.beans.Category;
import com.iteso.test.beans.City;
import com.iteso.test.beans.ItemProduct;
import com.iteso.test.beans.Store;

import java.util.ArrayList;

public class CategoryControl {
    public long addCategory(Category category, DataBaseHandler dh) {
        long inserted = 0;
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_CATEGORY_NAME, category.getName());
        // Inserting Row
        inserted = db.insert(DataBaseHandler.TABLE_CATEGORY, null, values);
        // Closing database connection
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        values = null;
        return inserted;
    }

    public void deleteCategory(int idCategory, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        db.delete(DataBaseHandler.TABLE_CATEGORY, DataBaseHandler.KEY_CATEGORY_ID + " = ?", new String[]{String.valueOf(idCategory)});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
    }

    public int updateCategory(Category category, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_CATEGORY_NAME, category.getName());
        // Updating row
        int count = db.update(DataBaseHandler.TABLE_CATEGORY, values, DataBaseHandler.KEY_CATEGORY_ID + " = ?", new String[]{String.valueOf(category.getIdCategory())});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        return count;
    }

    public Category getCategoryById(int idCategory, DataBaseHandler dh) {
        Category category = new Category();
        String selectQuery = "SELECT  C." + DataBaseHandler.KEY_CATEGORY_ID + "," + "C." + DataBaseHandler.KEY_CATEGORY_ID + "," + "C." + DataBaseHandler.KEY_CATEGORY_NAME + " FROM " + DataBaseHandler.TABLE_CATEGORY + " C WHERE C." + DataBaseHandler.KEY_CATEGORY_ID + "= " + idCategory;
        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            category.setIdCategory(cursor.getInt(0));
            category.setName(cursor.getString(1));
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }
        db = null;
        cursor = null;
        // return store
        return category;
    }

    public ArrayList<Category> getAllCategories(DataBaseHandler dh) {
        ArrayList<Category> categories = new ArrayList<Category>();
        String selectQuery;
        selectQuery = "SELECT C." + DataBaseHandler.KEY_CATEGORY_ID + ", " + "C." + DataBaseHandler.KEY_CATEGORY_NAME + " FROM " + DataBaseHandler.TABLE_CATEGORY + " C";

        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            Category category = new Category();
            category.setIdCategory(cursor.getInt(0));
            category.setName(cursor.getString(1));
            categories.add(category);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }

        db = null;
        cursor = null;
        // return store
        return categories;
    }
}
