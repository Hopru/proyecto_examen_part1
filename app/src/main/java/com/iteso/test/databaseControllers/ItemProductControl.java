package com.iteso.test.databaseControllers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iteso.test.DataBaseHandler;
import com.iteso.test.beans.Category;
import com.iteso.test.beans.City;
import com.iteso.test.beans.ItemProduct;
import com.iteso.test.beans.Store;

import java.util.ArrayList;

public class ItemProductControl {

    public long addItemProduct(ItemProduct product, DataBaseHandler dh) {
        long inserted = 0;
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_PRODUCT_IMAGE, product.getImage());
        values.put(DataBaseHandler.KEY_PRODUCT_TITLE, product.getTitle());
        values.put(DataBaseHandler.KEY_PRODUCT_CATEGORY, product.getCategory().getIdCategory());
        // Inserting Row
        inserted = db.insert(DataBaseHandler.TABLE_PRODUCT, null, values);
        //Insert product-store relationship
        values = new ContentValues();
        values.put(DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID, inserted);
        values.put(DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID, product.getStore().getId());
        inserted = db.insert(DataBaseHandler.TABLE_STORE_PRODUCT, null, values);
        // Closing database connection
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        values = null;
        return inserted;
    }

    public int updateProduct(ItemProduct product, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseHandler.KEY_PRODUCT_CATEGORY, product.getCategory().getIdCategory());
        values.put(DataBaseHandler.KEY_PRODUCT_IMAGE, product.getImage());
        values.put(DataBaseHandler.KEY_PRODUCT_TITLE, product.getTitle());
        // Updating row
        int count = db.update(DataBaseHandler.TABLE_PRODUCT, values, DataBaseHandler.KEY_PRODUCT_ID + " = ?", new String[]{String.valueOf(product.getId())});
        //Update product store table
        values = new ContentValues();
        values.put(DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID, product.getId());
        values.put(DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID, product.getStore().getId());
        count = db.update(DataBaseHandler.TABLE_STORE_PRODUCT, values, DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID + " = ?", new String[]{String.valueOf(product.getId())});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
        return count;
    }

    public void deleteProduct(int idProduct, DataBaseHandler dh) {
        SQLiteDatabase db = dh.getWritableDatabase();
        db.delete(DataBaseHandler.TABLE_STORE_PRODUCT, DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID + " = ?", new String[]{String.valueOf(idProduct)});
        db.delete(DataBaseHandler.TABLE_PRODUCT, DataBaseHandler.KEY_PRODUCT_ID + " = ?", new String[]{String.valueOf(idProduct)});
        try {
            db.close();
        } catch (Exception e) {
        }
        db = null;
    }

    public ItemProduct getProductById(int idProduct, DataBaseHandler dh) {
        ItemProduct itemProduct = new ItemProduct();

        String selectQuery = "SELECT P." + DataBaseHandler.KEY_PRODUCT_ID + ", " + "P." + DataBaseHandler.KEY_PRODUCT_TITLE + ", " + "P." + DataBaseHandler.KEY_PRODUCT_IMAGE + ", " + "C." + DataBaseHandler.KEY_CATEGORY_ID + ", C." + DataBaseHandler.KEY_CATEGORY_NAME +
                ", S." + DataBaseHandler.KEY_STORE_ID + ", S." + DataBaseHandler.KEY_STORE_LAT + ", S." + DataBaseHandler.KEY_STORE_LNG + ", S." + DataBaseHandler.KEY_STORE_NAME + ", S." + DataBaseHandler.KEY_STORE_PHONE + ", S." + DataBaseHandler.KEY_STORE_THUMBNAIL +
                ", CI." + DataBaseHandler.KEY_CITY_ID + ", CI." + DataBaseHandler.KEY_CITY_NAME +
                " FROM " + DataBaseHandler.TABLE_PRODUCT + " P, " + DataBaseHandler.TABLE_CATEGORY  + " C, " + DataBaseHandler.TABLE_STORE + " S, " + DataBaseHandler.TABLE_STORE_PRODUCT + " SP, " + DataBaseHandler.TABLE_CITY + " CI " +
                "WHERE " + "P." + DataBaseHandler.KEY_PRODUCT_ID +" = " +idProduct +
                " AND " + "P." + DataBaseHandler.KEY_PRODUCT_CATEGORY +" = C." + DataBaseHandler.KEY_CATEGORY_ID +
                " AND P." + DataBaseHandler.KEY_PRODUCT_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = CI." + DataBaseHandler.KEY_CITY_ID;
        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            itemProduct.setId(cursor.getInt(0));
            itemProduct.setTitle(cursor.getString(1));
            itemProduct.setImage(cursor.getInt(2));
            Category category = new Category();
            category.setIdCategory(cursor.getInt(3));
            category.setName(cursor.getString(4));
            itemProduct.setCategory(category);
            Store store = new Store();
            store.setId(cursor.getInt(5));
            store.setLatitude(cursor.getDouble(6));
            store.setLongitude(cursor.getDouble(7));
            store.setName(cursor.getString(8));
            store.setPhone(cursor.getString(9));
            store.setThumbnail(cursor.getInt(10));
            City city = new City();
            city.setIdCity(cursor.getInt(11));
            city.setName(cursor.getString(12));
            store.setCity(city);
            itemProduct.setStore(store);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }

        db = null;
        cursor = null;
        // return store
        return itemProduct;
    }

    public ArrayList<ItemProduct> getProductsWhere(String strWhere, String strOrderBy, DataBaseHandler dh) {
        ArrayList<ItemProduct> itemProducts = new ArrayList<ItemProduct>();
        String selectQuery;
        if(strWhere == null) {
            selectQuery = "SELECT P." + DataBaseHandler.KEY_PRODUCT_ID + ", " + "P." + DataBaseHandler.KEY_PRODUCT_TITLE + ", " + "P." + DataBaseHandler.KEY_PRODUCT_IMAGE + ", " + "C." + DataBaseHandler.KEY_CATEGORY_ID + ", C." + DataBaseHandler.KEY_CATEGORY_NAME +
                    ", S." + DataBaseHandler.KEY_STORE_ID + ", S." + DataBaseHandler.KEY_STORE_LAT + ", S." + DataBaseHandler.KEY_STORE_LNG + ", S." + DataBaseHandler.KEY_STORE_NAME + ", S." + DataBaseHandler.KEY_STORE_PHONE + ", S." + DataBaseHandler.KEY_STORE_THUMBNAIL +
                    ", CI." + DataBaseHandler.KEY_CITY_ID + ", CI." + DataBaseHandler.KEY_CITY_NAME +
                    " FROM " + DataBaseHandler.TABLE_PRODUCT + " P, " + DataBaseHandler.TABLE_CATEGORY  + " C, " + DataBaseHandler.TABLE_STORE + " S, " + DataBaseHandler.TABLE_STORE_PRODUCT + " SP, " + DataBaseHandler.TABLE_CITY + " CI" +
                    " WHERE P." + DataBaseHandler.KEY_PRODUCT_CATEGORY +" = C." + DataBaseHandler.KEY_CATEGORY_ID +
                    " AND P." + DataBaseHandler.KEY_PRODUCT_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID +
                    " AND S." + DataBaseHandler.KEY_STORE_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID +
                    " AND S." + DataBaseHandler.KEY_STORE_ID + " = CI." + DataBaseHandler.KEY_CITY_ID +
                    " ORDER BY P." + strOrderBy;
        } else {
            selectQuery = "SELECT P." + DataBaseHandler.KEY_PRODUCT_ID + ", " + "P." + DataBaseHandler.KEY_PRODUCT_TITLE + ", " + "P." + DataBaseHandler.KEY_PRODUCT_IMAGE + ", " + "C." + DataBaseHandler.KEY_CATEGORY_ID + ", C." + DataBaseHandler.KEY_CATEGORY_NAME +
                    ", S." + DataBaseHandler.KEY_STORE_ID + ", S." + DataBaseHandler.KEY_STORE_LAT + ", S." + DataBaseHandler.KEY_STORE_LNG + ", S." + DataBaseHandler.KEY_STORE_NAME + ", S." + DataBaseHandler.KEY_STORE_PHONE + ", S." + DataBaseHandler.KEY_STORE_THUMBNAIL +
                    ", CI." + DataBaseHandler.KEY_CITY_ID + ", CI." + DataBaseHandler.KEY_CITY_NAME +
                    " FROM " + DataBaseHandler.TABLE_PRODUCT + " P, " + DataBaseHandler.TABLE_CATEGORY  + " C, " + DataBaseHandler.TABLE_STORE + " S, " + DataBaseHandler.TABLE_STORE_PRODUCT + " SP, " + DataBaseHandler.TABLE_CITY + " CI" +
                    " WHERE P." + DataBaseHandler.KEY_PRODUCT_CATEGORY +" = C." + DataBaseHandler.KEY_CATEGORY_ID +
                    " AND P." + DataBaseHandler.KEY_PRODUCT_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID +
                    " AND S." + DataBaseHandler.KEY_STORE_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID +
                    " AND S." + DataBaseHandler.KEY_STORE_ID + " = CI." + DataBaseHandler.KEY_CITY_ID +
                    " AND "+ strWhere + " ORDER BY P." + strOrderBy;
        }
        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            ItemProduct itemProduct = new ItemProduct();
            itemProduct.setId(cursor.getInt(0));
            itemProduct.setTitle(cursor.getString(1));
            itemProduct.setImage(cursor.getInt(2));
            Category category = new Category();
            category.setIdCategory(cursor.getInt(3));
            category.setName(cursor.getString(4));
            itemProduct.setCategory(category);
            Store store = new Store();
            store.setId(cursor.getInt(5));
            store.setLatitude(cursor.getDouble(6));
            store.setLongitude(cursor.getDouble(7));
            store.setName(cursor.getString(8));
            store.setPhone(cursor.getString(9));
            store.setThumbnail(cursor.getInt(10));
            City city = new City();
            city.setIdCity(cursor.getInt(11));
            city.setName(cursor.getString(12));
            store.setCity(city);
            itemProduct.setStore(store);
            itemProducts.add(itemProduct);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }

        db = null;
        cursor = null;
        // return store
        return itemProducts;
    }

    public ArrayList<ItemProduct> getProductsByCategory(String categoryName, DataBaseHandler dh) {
        ArrayList<ItemProduct> itemProducts = new ArrayList<ItemProduct>();
        String selectQuery;
        selectQuery = "SELECT P." + DataBaseHandler.KEY_PRODUCT_ID + ", " + "P." + DataBaseHandler.KEY_PRODUCT_TITLE + ", " + "P." + DataBaseHandler.KEY_PRODUCT_IMAGE + ", " + "C." + DataBaseHandler.KEY_CATEGORY_ID + ", C." + DataBaseHandler.KEY_CATEGORY_NAME +
                ", S." + DataBaseHandler.KEY_STORE_ID + ", S." + DataBaseHandler.KEY_STORE_LAT + ", S." + DataBaseHandler.KEY_STORE_LNG + ", S." + DataBaseHandler.KEY_STORE_NAME + ", S." + DataBaseHandler.KEY_STORE_PHONE + ", S." + DataBaseHandler.KEY_STORE_THUMBNAIL +
                ", CI." + DataBaseHandler.KEY_CITY_ID + ", CI." + DataBaseHandler.KEY_CITY_NAME +
                " FROM " + DataBaseHandler.TABLE_PRODUCT + " P, " + DataBaseHandler.TABLE_CATEGORY  + " C, " + DataBaseHandler.TABLE_STORE + " S, " + DataBaseHandler.TABLE_STORE_PRODUCT + " SP, " + DataBaseHandler.TABLE_CITY + " CI" +
                " WHERE P." + DataBaseHandler.KEY_PRODUCT_CATEGORY +" = C." + DataBaseHandler.KEY_CATEGORY_ID +
                " AND P." + DataBaseHandler.KEY_PRODUCT_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = CI." + DataBaseHandler.KEY_CITY_ID +
                " AND C." + DataBaseHandler.KEY_CATEGORY_NAME + " = " + categoryName;


        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            ItemProduct itemProduct = new ItemProduct();
            itemProduct.setId(cursor.getInt(0));
            itemProduct.setTitle(cursor.getString(1));
            itemProduct.setImage(cursor.getInt(2));
            Category category = new Category();
            category.setIdCategory(cursor.getInt(3));
            category.setName(cursor.getString(4));
            itemProduct.setCategory(category);
            Store store = new Store();
            store.setId(cursor.getInt(5));
            store.setLatitude(cursor.getDouble(6));
            store.setLongitude(cursor.getDouble(7));
            store.setName(cursor.getString(8));
            store.setPhone(cursor.getString(9));
            store.setThumbnail(cursor.getInt(10));
            City city = new City();
            city.setIdCity(cursor.getInt(11));
            city.setName(cursor.getString(12));
            store.setCity(city);
            itemProduct.setStore(store);
            itemProducts.add(itemProduct);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
        }

        db = null;
        cursor = null;
        // return store
        return itemProducts;
    }

    public static Cursor getProductsByCategoryCursor(String categoryName, DataBaseHandler dh) {
        String selectQuery;
        selectQuery = "SELECT P." + DataBaseHandler.KEY_PRODUCT_ID + ", " + "P." + DataBaseHandler.KEY_PRODUCT_TITLE + ", " + "P." + DataBaseHandler.KEY_PRODUCT_IMAGE + ", " + "C." + DataBaseHandler.KEY_CATEGORY_ID + ", C." + DataBaseHandler.KEY_CATEGORY_NAME +
                ", S." + DataBaseHandler.KEY_STORE_ID + ", S." + DataBaseHandler.KEY_STORE_LAT + ", S." + DataBaseHandler.KEY_STORE_LNG + ", S." + DataBaseHandler.KEY_STORE_NAME + ", S." + DataBaseHandler.KEY_STORE_PHONE + ", S." + DataBaseHandler.KEY_STORE_THUMBNAIL +
                ", CI." + DataBaseHandler.KEY_CITY_ID + ", CI." + DataBaseHandler.KEY_CITY_NAME +
                " FROM " + DataBaseHandler.TABLE_PRODUCT + " P, " + DataBaseHandler.TABLE_CATEGORY  + " C, " + DataBaseHandler.TABLE_STORE + " S, " + DataBaseHandler.TABLE_STORE_PRODUCT + " SP, " + DataBaseHandler.TABLE_CITY + " CI" +
                " WHERE P." + DataBaseHandler.KEY_PRODUCT_CATEGORY +" = C." + DataBaseHandler.KEY_CATEGORY_ID +
                " AND P." + DataBaseHandler.KEY_PRODUCT_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_PRODUCT_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = SP." + DataBaseHandler.KEY_STORE_PRODUCT_STORE_ID +
                " AND S." + DataBaseHandler.KEY_STORE_ID + " = CI." + DataBaseHandler.KEY_CITY_ID +
                " AND C." + DataBaseHandler.KEY_CATEGORY_NAME + " = " + categoryName;


        SQLiteDatabase db = dh.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
//        while (cursor.moveToNext()) {
//            ItemProduct itemProduct = new ItemProduct();
//            itemProduct.setId(cursor.getInt(0));
//            itemProduct.setTitle(cursor.getString(1));
//            itemProduct.setImage(cursor.getInt(2));
//            Category category = new Category();
//            category.setIdCategory(cursor.getInt(3));
//            category.setName(cursor.getString(4));
//            itemProduct.setCategory(category);
//            Store store = new Store();
//            store.setId(cursor.getInt(5));
//            store.setLatitude(cursor.getDouble(6));
//            store.setLongitude(cursor.getDouble(7));
//            store.setName(cursor.getString(8));
//            store.setPhone(cursor.getString(9));
//            store.setThumbnail(cursor.getInt(10));
//            City city = new City();
//            city.setIdCity(cursor.getInt(11));
//            city.setName(cursor.getString(12));
//            store.setCity(city);
//            itemProduct.setStore(store);
//            itemProducts.add(itemProduct);
//        }

        return cursor;
    }
}
