package com.iteso.test;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.SyncStateContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.iteso.test.beans.ItemProduct;
import com.iteso.test.tools.Commons;
import com.iteso.test.tools.Constants;

public class ActivityMain extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    FragmentTechology fragmentTechnology;
    FragmentHome fragmentHome;
    FragmentElectronics fragmentElectronics;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.INTENT_PRODUCTS_NOTIFY:
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        ItemProduct itemProduct = data.getParcelableExtra("ITEM");
                        if (itemProduct.getCategory().getName().equalsIgnoreCase("TECHNOLOGY")) {
                            fragmentTechnology.notifyDataSetChanged(itemProduct);
                        } else if(itemProduct.getCategory().getName().equalsIgnoreCase("HOME")) {
                            fragmentHome.notifyDataSetChanged(itemProduct);
                        } else if(itemProduct.getCategory().getName().equalsIgnoreCase("ELECTRONICS")) {
                            fragmentElectronics.notifyDataSetChanged(itemProduct);
                        }
                    }
                }
                break;
        }
        /*
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                fragmentTechnology.onActivityResult(requestCode, resultCode, data);
            }
        }
        */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Intent intent;
        switch (id) {
            case R.id.action_settings:
                break;
            case R.id.action_privacy:
                intent = new Intent(ActivityMain.this, ActivityPrivacyPolicy.class);
                startActivity(intent);
                break;
            case R.id.action_logout:
                SharedPreferences sharedPreferences = getSharedPreferences(
                        ActivitySplashScreen.MY_PREFERENCES, MODE_PRIVATE
                );
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                intent = new Intent(ActivityMain.this, ActivityLogin.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_products:
                Intent products = new Intent(this, ActivityProduct.class);
                startActivityForResult(products, Constants.INTENT_PRODUCTS_NOTIFY);
                break;
            default:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (fragmentTechnology == null) {
                        fragmentTechnology = new FragmentTechology();
                    }
                    return fragmentTechnology;
                case 1:
                    if(fragmentHome == null) {
                        fragmentHome = new FragmentHome();
                    }
                    return fragmentHome;
                case 2:
                    if(fragmentElectronics == null) {
                        fragmentElectronics =  new FragmentElectronics();
                    }
                    return fragmentElectronics;
                default:
                    if (fragmentTechnology == null) {
                        fragmentTechnology = new FragmentTechology();
                    }
                    return fragmentTechnology;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return Commons.PAGES;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case Commons.IDX_SECTION1:
                    return getString(R.string.section_1);
                case Commons.IDZ_SECTION2:
                    return getString(R.string.section_2);
                case Commons.IDX_SECTION3:
                    return getString(R.string.section_3);
            }
            return null;
        }
    }


}
