package com.iteso.test.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;

public class ItemProduct implements Parcelable {
    private int image;
    private String title;
    private Store store;
    private Category category;
    private String description;
    private int code;
    private Integer id;

    @Override
    public String toString() {
        return "ItemProduct{" +
                "image=" + image +
                ", title='" + title + '\'' +
                ", store=" + store +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", code=" + code +
                ", id=" + id +
                '}';
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.image);
        dest.writeString(this.title);
        dest.writeParcelable(this.store, flags);
        dest.writeParcelable(this.category, flags);
        dest.writeString(this.description);
        dest.writeInt(this.code);
        dest.writeValue(this.id);
    }

    public ItemProduct() {
    }

    protected ItemProduct(Parcel in) {
        this.image = in.readInt();
        this.title = in.readString();
        this.store = in.readParcelable(Store.class.getClassLoader());
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.description = in.readString();
        this.code = in.readInt();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<ItemProduct> CREATOR = new Creator<ItemProduct>() {
        @Override
        public ItemProduct createFromParcel(Parcel source) {
            return new ItemProduct(source);
        }

        @Override
        public ItemProduct[] newArray(int size) {
            return new ItemProduct[size];
        }
    };
}
